package epsi.b3.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebElement;
import java.util.List;




public class CommanderpizzaTest {

	private WebDriver webDriver;

	@Before
	public void createWebDriver() {
		webDriver = new ChromeDriver();
	}

	@After
	public void closeWebDriver() {
		webDriver.quit();
	}

	@Test
	public void commanderPizzaReussi() throws Exception {
		
	    webDriver.navigate().to("http://localhost:8081/pizza-spring");
	    
		/*
		 * List<WebElement> resultLinks =
		 * webDriver.findElements(By.partialLinkText("https://www.selenium.dev"));
		 * assertFalse("Aucun lien trouvé pour selenium.dev", resultLinks.isEmpty());
		 */
		
	    WebElement element = (new WebDriverWait(webDriver, 10))
	    		   .until(ExpectedConditions.elementToBeClickable(By.linkText("Commander")));
	    element.click();
	    webDriver.findElement(By.linkText("Commander"));
	    
	    WebElement element2 = (new WebDriverWait(webDriver, 10))
	    		   .until(ExpectedConditions.elementToBeClickable(By.id("pizzaId")));
	    webDriver.findElement(By.id("pizzaId")).sendKeys("Regina");
	    webDriver.findElement(By.id("nom")).sendKeys("Laulau");
	    webDriver.findElement(By.id("telephone")).sendKeys("0450546652");
	    webDriver.findElement(By.id("valider")).click();
	   
	    
	}
	
	@Test
	public void commanderPizzaSansPizza() throws Exception {
		
	    webDriver.navigate().to("http://localhost:8081/pizza-spring");
	    
	    WebElement element = (new WebDriverWait(webDriver, 10))
	    		   .until(ExpectedConditions.elementToBeClickable(By.linkText("Commander")));
	   
	    webDriver.findElement(By.linkText("Commander"));
	    webDriver.findElement(By.id("nom")).sendKeys("Laulau");
	    webDriver.findElement(By.id("telephone")).sendKeys("0450546652");
	    webDriver.findElement(By.cssSelector("button")).click();;
	    
	    WebElement result = webDriver.findElement(By.id("recap-commande"));  
	    assertFalse("Vous devez choisir au moins une pizza", result.isDisplayed());


	     
	    
	}
	
	@Test
	public void commanderPizzaSansNumeroDeTelephone() throws Exception {
		
	    webDriver.navigate().to("http://localhost:8081/pizza-spring");
	    
	    WebElement element = (new WebDriverWait(webDriver, 10))
	    		   .until(ExpectedConditions.elementToBeClickable(By.linkText("Commander")));
	    
	 
	    webDriver.findElement(By.linkText("Commander"));
	    
	    webDriver.findElement(By.id("pizzaId")).sendKeys("Regina");
	    webDriver.findElement(By.id("nom")).sendKeys("Laulau");
	    webDriver.findElement(By.id("valider")).click();
	    
	    WebElement result = webDriver.findElement(By.id("recap-commande"));  
	    assertFalse("ne peut pas être vide", result.isDisplayed());
	   


	     
	  
	}

}
