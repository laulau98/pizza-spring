// Generated by Selenium IDE
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import java.util.*;
import java.net.MalformedURLException;
import java.net.URL;




public class CommanderpizzaTest {
	
  private WebDriver driver;
  private Map<String, Object> vars;
  JavascriptExecutor js;
  
  @Before
  public void setUp() {
    driver = new ChromeDriver();
    js = (JavascriptExecutor) driver;
    vars = new HashMap<String, Object>();
  }
  
  @After
  public void tearDown() {
    driver.quit();
  }
  
  @Test
  public void commandersansselectionnerunepizza() {
    driver.get("http://localhost:8081/pizza-spring");
    driver.manage().window().setSize(new Dimension(784, 824));
    driver.findElement(By.id("menu")).click();
    {
      List<WebElement> elements = driver.findElements(By.id("menu"));
      assert(elements.size() > 0);
    }
    driver.findElement(By.linkText("Commander")).click();
    {
      List<WebElement> elements = driver.findElements(By.id("commande"));
      assert(elements.size() > 0);
    }
    driver.findElement(By.id("nom")).sendKeys("Laulau");
    {
      String value = driver.findElement(By.id("nom")).getAttribute("value");
      assertThat(value, is("Laulau"));
    }
    driver.findElement(By.id("telephone")).sendKeys("12364");
    {
      String value = driver.findElement(By.id("telephone")).getAttribute("value");
      assertThat(value, is("12364"));
    }
    driver.findElement(By.cssSelector("button")).click();
    {
      List<WebElement> elements = driver.findElements(By.id("commande"));
      assert(elements.size() > 0);
    }
  }
  
  
  @Test
  public void commanderpizzareussie() {
    driver.get("http://localhost:8081/pizza-spring");
    driver.manage().window().setSize(new Dimension(786, 824));
    driver.findElement(By.linkText("Commander")).click();
    {
      List<WebElement> elements = driver.findElements(By.id("commande"));
      assert(elements.size() > 0);
    }
    {
      WebElement dropdown = driver.findElement(By.id("pizzaId"));
      dropdown.findElement(By.xpath("//option[. = 'Trois fromages']")).click();
    }
    {
      String value = driver.findElement(By.id("pizzaId")).getAttribute("value");
      assertThat(value, is("3"));
    }
    driver.findElement(By.id("nom")).sendKeys("laulau");
    {
      String value = driver.findElement(By.id("nom")).getAttribute("value");
      assertThat(value, is("laulau"));
    }
    driver.findElement(By.id("telephone")).sendKeys("23564");
    {
      String value = driver.findElement(By.id("telephone")).getAttribute("value");
      assertThat(value, is("23564"));
    }
    driver.findElement(By.cssSelector("button")).click();
    {
      List<WebElement> elements = driver.findElements(By.id("recap-commande"));
      assert(elements.size() > 0);
    }
  }
  
  
  @Test
  public void commandersansfournirunnumerodetelephone() {
    driver.get("http://localhost:8081/pizza-spring");
    driver.manage().window().setSize(new Dimension(787, 666));
    driver.findElement(By.id("menu")).click();
    {
      List<WebElement> elements = driver.findElements(By.id("menu"));
      assert(elements.size() > 0);
    }
    driver.findElement(By.linkText("Commander")).click();
    {
      WebElement dropdown = driver.findElement(By.id("pizzaId"));
      dropdown.findElement(By.xpath("//option[. = 'Regina']")).click();
    }
    {
      String value = driver.findElement(By.id("pizzaId")).getAttribute("value");
      assertThat(value, is("2"));
    }
    driver.findElement(By.id("nom")).sendKeys("laulau");
    {
      String value = driver.findElement(By.id("nom")).getAttribute("value");
      assertThat(value, is("laulau"));
    }
    driver.findElement(By.cssSelector("button")).click();
    {
      List<WebElement> elements = driver.findElements(By.id("commande"));
      assert(elements.size() > 0);
    }
  }
}
